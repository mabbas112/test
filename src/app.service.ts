import { Injectable } from '@nestjs/common';
import { sql } from '@vercel/postgres';

@Injectable()
export class AppService {
  async getHello() {
    try {
      const petName = 'Cat';
      const ownerName = 'Will';
      if (!petName || !ownerName)
        throw new Error('Pet and owner names required');
      await sql`INSERT INTO Pets (Name, Owner) VALUES (${petName}, ${ownerName});`;
      return 'Deployment Successful, connected to database!';
    } catch (error) {
      return 'error';
    }
  }
}
